/**
 * Задание 12 - Создать интерфейс StarWars DB для данных из SWAPI.
 *
 * Используя SWAPI, вывести информацию по всех планетам с пагинацией и возможностью просмотреть доп.
 * информацию в модальном окне с дозагрузкой смежных ресурсов из каждой сущности.
 *
 * Данные для отображения в карточке планеты:
 * 1. Наименование (name)
 * 2. Диаметр (diameter)
 * 3. Население (population)
 * 4. Уровень гравитации (gravity)
 * 5. Природные зоны (terrain)
 * 6. Климатические зоны (climate)
 *
 * При клике по карточке отображаем в модальном окне всю информацию
 * из карточки, а также дополнительную:
 * 1. Список фильмов (films)
 * - Номер эпизода (episode_id)
 * - Название (title)
 * - Дата выхода (release_date)
 * 2. Список персонажей (residents)
 * - Имя (name)
 * - Пол (gender)
 * - День рождения (birth_year)
 * - Наименование родного мира (homeworld -> name)
 *
 * Доп. требования к интерфейсу:
 * 1. Выводим 10 карточек на 1 странице
 * 2. Пагинация позволяет переключаться между страницами, выводить общее количество страниц и текущие выбранные
 * элементы в формате 1-10/60 для 1 страницы или 11-20/60 для второй и т.д.
 * 3. Используем Bootstrap для создания интерфейсов.
 * 4. Добавить кнопку "Показать все" - по клику загрузит все страницы с планетами и выведет
 * информацию о них в един (опцианально)
 */
// ======= доделать
/**
 * Добавить кнопку "Показать все" - по клику загрузит все страницы с планетами и выведет информацию о них в един (опцианально)
 * Previous/Next
 */

function getPlanets(page = 1) { 
    return fetch(`https://swapi.dev/api/planets/?page=${page}`).then(res => res.json())
        .then(res => ({total: res.count, entities: res.results}))

}

function getPlanet(url) {
    return fetch(url).then(res => res.json())
}


async function getDataForModal(planetUrl) {

    const planet = await getPlanet(planetUrl);
    const filmsPromise = planet.films.map(filmsUrl => fetch(filmsUrl).then(res => res.json()));
    const films = await Promise.all(filmsPromise);

    const residentsPromise = planet.residents.map(residentUrl => fetch(residentUrl).then(res => res.json()));
    const residents = await Promise.all(residentsPromise);

    const newHomeWorld = await Promise.all(residents.map(resident => fetch(resident.homeworld).then(res => res.json())));

    residents.forEach((value, key, map) => {
        residents[key].homeworld = newHomeWorld[key].name;
    });
    planetName = planet.name;

    return {films, residents, planetName};
    }

const getAllPlanets = async () => {
    const url = 'https://swapi.dev/api/planets/';   
    const res = await fetch(url);                  
    const { count, results } = await res.json();   
    const resultsCount = results.length;                          
    const pages = [                                                   
        results,                                                      
        ...await Promise.all([                                        
            ...new Array(Math.ceil(count / resultsCount) - 1).keys()    
        ]
            .map(async n => {
                const page = n + 2                                     
                const res = await fetch(`${url}?page=${page}`)       

                return (await res.json()).results
            }))
    ]
    return result = pages.flat();                                         
}

(async () => {
    const PAGE_SIZE = 10;
    const planets = await getPlanets();
    renderPage(planets.entities);
    renderPaginator(planets.total, PAGE_SIZE);
    managePage();
    manageModal();
    prevNextPaginator();
    loadAllPlanets();
    returnPlanets();

})()

function renderCard(planetInfo) {
    const items = ['diameter', 'population', 'gravity', 'terrain', 'climate']
        .map(key => `<li>${key}: ${planetInfo[key]}</li>`).join('');

    return `
    <div class="card">
    <div class="card-body">
    <h5 class="card-title">${planetInfo.name}</h5>
    <ul>${items}</ul>
    <button type="button" data-link="${planetInfo.url}" class="btn btn-primary js-open-modal" data-bs-toggle="modal" data-bs-target="#exampleModal">
    More Information</button>
    </div>
    </div>
    `;
}

function renderPage(planets) {
    const cards = planets.map(planet => renderCard(planet)).join('');
    document.querySelector('.js-cards').innerHTML = cards;
}

function renderPaginator(total, pageSize) {
    const countPages = Math.ceil(total / pageSize);
    let items = '';
    for (let i = 0; i < countPages; i++) {
        items += `<li class="page-item"><a class="page-link" href="#">${i + 1}</a></li>`
    }

    const paginatorHtml = `
    <ul class="pagination">
    <li class="page-item "><a  class="page-link previous" href="#">Previous</a></li>
    ${items}
    <li class="page-item "><a  class="page-link next" href="#" >Next</a></li>
    </ul>
     <br>
     <button type="button" class="btn btn-info js-btn-allPlanet ms-3">Показать все планеты</button>
`;

    document.querySelector('.js-pagination').innerHTML = paginatorHtml;
    document.querySelectorAll('.page-item')[1].classList.add('active');
    document.querySelector('.previous').parentNode.classList.add('disabled');

}

function managePage() {
    document.querySelector('.js-pagination').addEventListener('click', async event => {
        if (!event.target.classList.contains('page-link')) {
            return
        }

        if (event.target.classList.contains('page-link')) {
            if (event.target.classList.contains('previous') || event.target.classList.contains('next')) {
                return
            }
            let currentPage = Number(event.target.textContent);
            const planetsInfo = await getPlanets(currentPage); 
            renderPage(planetsInfo.entities);

            document.querySelectorAll('.page-item').forEach(itemEl => itemEl.classList.remove('active'));
            event.target.parentNode.classList.add('active'); 
            document.querySelectorAll('.page-item').forEach(itemEl => itemEl.classList.remove('disabled'));

            const nextBtn=document.querySelector('.next');
            const  prevBtn=document.querySelector('.previous');
            const countPages = (document.querySelectorAll('.page-link').length) - 2;
            console.log(currentPage);
            console.log(countPages);
            if ( currentPage === 1){
                prevBtn.parentNode.classList.add('disabled');
            } else if ( currentPage === countPages){
                nextBtn.parentNode.classList.add('disabled');
            }
        }
    });
}

function prevNextPaginator() {
    const countPages = (document.querySelectorAll('.page-link').length) - 2;
    const nextBtn=document.querySelector('.next');
    const prevBtn=document.querySelector('.previous');


    nextBtn.addEventListener('click', async event => {
        document.querySelectorAll('.page-item').forEach(itemEl => itemEl.classList.remove('disabled'));
        let activeBtn = document.querySelector('.active');
        let currentPage = Number(activeBtn.textContent);
        if (currentPage === countPages) {
            nextBtn.parentNode.classList.add('disabled');
        } else if (currentPage < countPages) {
            currentPage++;
            let planetsInfo = await getPlanets(currentPage);
            renderPage(planetsInfo.entities);
            activeBtn.nextElementSibling.classList.add('active');
            activeBtn.classList.remove('active');
            if (currentPage === countPages) {
                nextBtn.parentNode.classList.add('disabled');
            }
        }

    });

    prevBtn.addEventListener('click', async event => {
        document.querySelectorAll('.page-item').forEach(itemEl => itemEl.classList.remove('disabled'));
        let activeBtn = document.querySelector('.active');
        let currentPage = Number(activeBtn.textContent);
        if (currentPage === 1) {
            prevBtn.parentNode.classList.add('disabled');
        } else {
            currentPage--;
            let planetsInfo = await getPlanets(currentPage);
            renderPage(planetsInfo.entities);
            activeBtn.previousElementSibling.classList.add('active');
            activeBtn.classList.remove('active');
            if (currentPage === 1) {
                prevBtn.parentNode.classList.add('disabled');
            }

        }

    });
}

function manageModal() {
    document.querySelector('.js-cards').addEventListener('click', async event => {
        if (!event.target.classList.contains('js-open-modal')) {
            return
        }

        const url = event.target.getAttribute('data-link');

        document.querySelector('.js-modal-body').innerHTML =
            `    <div class="spinner-border" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>`

        const {films, residents, planetName} = await getDataForModal(url);

        const filmsList = films.map(
            film => [{key: 'episode_id', label: 'Номер эпизода'}, {
                key: 'title',
                label: 'Название'
            }, {key: 'release_date', label: 'Дата выхода'}]
                .map(({key, label}) => `<li>${label}: ${film[key]}</li>`).join('')).join('');

        const residentsList = residents.map(
            resident => ['name', 'gender', 'birth_year','homeworld']
                .map(key => `<li>${key}: ${resident[key]}</li>`).join('')).join('');

        document.querySelector('.modal-title').innerHTML = `${planetName}`;
        document.querySelector('.js-modal-body').innerHTML = `
        <h6>Фильмы<h6>
        <ul>${filmsList}</ul>
         <h6>Персонажи<h6>
        <ul>${residentsList}</ul>
        `;
    })
}

function loadAllPlanets() {
    let loadBtn = document.querySelector('.js-btn-allPlanet');                
    loadBtn.addEventListener('click', async event => {                    
        loadBtn.innerHTML = `<div class="spinner-border" role="status">         
        <span class="visually-hidden">Loading...</span></div>`;                 
        const allPlanets = await getAllPlanets();                            
        renderPage(allPlanets);                                              
        document.querySelector('.js-pagination').innerHTML = '';     
        const returnPlanetHtml = `
         <br>
     <button type="button" class="btn btn-info js-btn-returnPlanet ms-3">Вернуться назад к страницам</button>
`;
        document.querySelector('.js-title').innerHTML = 'Planets UI' + returnPlanetHtml;
    });

    const returnBtn = document.querySelector('.js-title');
    returnBtn.addEventListener('click', async event => {
        if (event.target.classList.contains('js-btn-returnPlanet')) { 
            console.log('Привет');
            returnBtn.innerHTML = `<div class="spinner-border" role="status">
        <span class="visually-hidden">Loading...</span></div>`;
            const planets = await getPlanets(1);
            renderPage(planets.entities);
            renderPaginator(planets.total, 10);
            document.querySelector('.js-title').innerHTML = 'Planets UI';
        }
    });
}


